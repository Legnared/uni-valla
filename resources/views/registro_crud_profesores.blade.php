@extends('layouts.plantilla-menu')
@section('content')
    <div class="container">
      @if (session()->has('profe'))
        <div  class="">{{session('profe')}}</div>  <!--Darle estilo a este cuadro, que aparece despues de un registro correcto-->
      @endif
        <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Crear Profesor</h1>
                  </div>
                  <form class="user" method="POST" role="form" action="{{ route('almacenar_profesor') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input  required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}" title="Formato: De 3 a 30 letras" type="text" value="{{old('nombre')}}"   name="nombre" class="form-control form-control-user"  autocomplete="off" id="nombre" placeholder="Nombre(s)">
                            {{$errors->first('nombre')}}
                    </div>
                    <div class="form-group">
                        <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}" title="Formato: De 3 a 45 letras" required class="form-control form-control-user" name="apP" id="apP" autocomplete="off" placeholder="Apellido Paterno">
                    </div>
                    <div class="form-group">
                        <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,15}" autocomplete="off" title="Formato: De 3 a 45 letras" required class="form-control form-control-user" id="apM" name="apM" placeholder="Apellido Materno">
                    </div>
                    <div class="form-group">
                        <input type="text" pattern="[0-9]{7,10}" title="Formato: De 7 a 10 números" autocomplete="off" required class="form-control form-control-user" name="telefono" id="telefono" placeholder="Telefono">
                    </div>
                    <div class="form-group">
                        <h3>Sexo</h3>
                        <div class="radio">
                            <label><input type="radio" name="sexo" value="M" checked>M</label>
                        </div>
                        <div class="radio">
                             <label><input type="radio" value="F" name="sexo">F</label>
                         </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" autocomplete="off" name="cedula" id="cedula" placeholder="Cedula profesional">
                    </div>
                    <div class="form-group">
                      <input type="email"  title="Máximo 35 caracteres" autocomplete="off" class="form-control form-control-user" name="correo" id="correo" placeholder="Correo electronico">
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Contraseña">
                      </div>
                      <div class="col-sm-6">
                        <input type="password" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Repetir Contraseña">
                      </div>
                    </div>
                      <input type="hidden" name="estatus" id="Estatus" value="1" class="form-control input-sm" placeholder="Estatus">
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                      Registrar
                    </button>
                    <hr>
                  </form>

    </div>
@endsection
