@extends('layouts.plantilla-menu')
@section('content')
    <div class="container">
        <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Crear Usuario Alumno</h1>
                  </div>
                  <form class="user">
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" id="exampleFirstName" placeholder="Nombre(s)">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" id="exampleLastName" placeholder="Apellido Paterno">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" id="exampleLastName" placeholder="Apellido Materno">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" id="exampleLastName" placeholder="Telefono">
                    </div>
                    <div class="form-group">
                        <h3>Sexo</h3>
                        <div class="radio">
                            <label><input type="radio" name="optradio" checked>M</label>
                        </div>
                        <div class="radio">
                             <label><input type="radio" name="optradio">F</label>
                         </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" id="exampleLastName" placeholder="Clave de Usuario">
                    </div>
                    <div class="form-group">
                      <input type="email" class="form-control form-control-user" id="exampleInputEmail" placeholder="Correo electronico">
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Contraseña">
                      </div>
                      <div class="col-sm-6">
                        <input type="password" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Repetir Contraseña">
                      </div>
                    </div>
                    <a href="#" class="btn btn-primary btn-user btn-block">
                      Registrar
                    </a>
                    <hr>
                  </form>
    </div>
    <script type="text/javascript">
           $(function () {
               $('#datetimepicker5').datetimepicker({
                   defaultDate: "11/1/2013",
                   disabledDates: [
                       moment("12/25/2013"),
                       new Date(2013, 11 - 1, 21),
                       "11/22/2013 00:53"
                   ]
               });
           });
       </script>
@endsection
