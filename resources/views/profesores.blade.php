@extends('layouts.plantilla-menu')
@section('content')
    <div class="container">

        <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Crear Profesor</h1>
                  </div>
                  <form class="user" method="POST" role="form" action="{{ route('almacenar_profesor') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text"  name="nombre" class="form-control form-control-user" id="nombre" placeholder="Nombre(sss)">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="apP" id="apP" placeholder="Apellido Paterno">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" id="apM" name="apM" placeholder="Apellido Materno">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="telefono" id="telefono" placeholder="Telefono">
                    </div>
                    <div class="form-group">
                        <h3>Sexo</h3>
                        <div class="radio">
                            <label><input type="radio" name="sexo" value="M" checked>M</label>
                        </div>
                        <div class="radio">
                             <label><input type="radio" value="S" name="sexo">F</label>
                         </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="cedula" id="cedula" placeholder="Cedula profesional">
                    </div>
                    <div class="form-group">
                      <input type="email" class="form-control form-control-user" name="correo" id="correo" placeholder="Correo electronico">
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Contraseña">
                      </div>
                      <div class="col-sm-6">
                        <input type="password" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Repetir Contraseña">
                      </div>
                    </div>
                      <input type="hidden" name="estatus" id="Estatus" value="1" class="form-control input-sm" placeholder="Estatus">
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                      Registrara
                    </button>
                    <hr>
                  </form>

    </div>
@endsection
