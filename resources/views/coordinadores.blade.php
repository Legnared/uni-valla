@extends('layouts.plantilla-menu')
@section('content')
	<div class="container">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif

			
				<div class="text-center">
					<h1 class="h4 text-gray-900 mb-4">Nuevo Coordindador</h1>
				</div>
				
						<form method="POST" action="{{ route('coordinador.store') }}"  role="form">
							{{ csrf_field() }}
							

							
									<div class="form-group">
										<input type="text" name="Nombre" id="Nombre" class="form-control form-control-user" placeholder="Nombre">
									</div>

								

							
									<div class="form-group">
										<input type="text" name="Ap_Paterno" id="Ap_Paterno" class="form-control form-control-user" placeholder="Apellido Paterno">
									</div>



                  <div class="form-group">
                    <input type="text" name="Ap_Materno" id="Ap_Materno" class="form-control form-control-user" placeholder="Apellido Materno">
                  </div>





							

							    <div class="form-group">
										<input type="tel" name="Telefono" id="Telefono" class="form-control form-control-user" placeholder="Telefono">
									</div>
							
									<div class="form-group">
										<input type="email" name="Correo" id="Correo" class="form-control form-control-user" placeholder="Correo">
									</div>
						

							<div class="form-goup" id="Sexo">
                  <h3>Sexo          </h3> 
                  <div class="radio">
                    <label><input type="radio" id="Sexo" name="Sexo" value="M" checked>Masculino  </label><br>
                  </div>
                  <div class="radio">
                    <label><input type="radio" id="Sexo" name="Sexo"  value="F">Femenino  </label>
                  </div>
              </div>

                <div class="form-group">
                  <input type="password" id="Password" name="Password" class="form-control form-control-user"  placeholder="Contraseña">
                </div>
                <div class="form-group">
                  <input type="password" id="Password1" name="Password1" class="form-control form-control-user"  placeholder="Repetir Contraseña">
                </div>
						




								<div class="col-xs-12 col-sm-12 col-md-12">
									<input type="submit"  value="Registrar" class="btn btn-primary btn-user btn-block">
								</div>
            

							</div>
						</form>
				

			
		
	</section>
	@endsection
