
@extends('layouts.layout')
@section('stylesheet')
  <link href="{{{ asset('css/usuarios.css') }}}" rel="stylesheet">
@endsection
@section('title', 'Plan de Estudios')
@section('content')
<div class="row">
  <section class="content">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="pull-left"><h3>Planes de Estudio</h3></div>
          <div class="pull-right">
            <div class="btn-group">
              <a href="{{ route('plan_estudios.create') }}" class="btn btn-info" >Añadir Plan</a>
            </div>
          </div>
          <div class="table-container">
            <table id="mytable" class="table table-bordred table-striped">
             <thead>
               <th>ID</th>
               <th>Descripcion</th>
               <th>Carrera</th>
               <th>Materias</th>
                <th>Editar</th>
             </thead>
             <tbody>
              @if($plan_estudios->count())
              @foreach($plan_estudios as $plan_estudio)
              <tr>
                <td>{{$plan_estudio->Id_plan}}</td>
                <td>{{$plan_estudio->Descripcion}}</td>
                <td>{{$plan_estudio->id_Carrera}}</td>
                <td>{{$plan_estudio->MATERIA_Clave_Materia}}</td>

                <td><a class="btn btn-primary btn-xs" href="{{action('PlanEstudiosController@edit', $plan_estudio->Id_plan)}}" ><span class="glyphicon glyphicon-pencil"></span></a></td>

               </tr>
               @endforeach
               @else
               <tr>
                <td colspan="8">No hay registros !!</td>
              </tr>
              @endif
            </tbody>

          </table>
        </div>
      </div>
      {{ $plan_estudios->links() }}
    </div>
  </div>
</section>

@endsection
