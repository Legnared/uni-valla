@extends('layouts.plantilla-menu')
@section('content')
@if (session()->has('profe'))
  <div  class="">{{session('profe')}}</div>  <!--Darle estilo a este cuadro, que aparece despues de eliminar un registro correcto-->
@endif
<div class="main-content">
    <div class="section__content section__content--p1250">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <h3 class="title-12 m-b-12">Tabla de profesores</h3>
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                        </div>
                        <div class="table-data__tool-right">
                          <a href="{{ route('crear_profesor') }}"><button class="au-btn au-btn-icon au-btn--green au-btn--small">
                            <i class="zmdi zmdi-plus"></i>Agregar Profesor</button></a>
                            <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                <!--<select class="js-select2" name="type">
                                    <option selected="selected">Export</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                </select>-->

                            </div>
                        </div>
                    </div>
                    <!-- Start Table -->
                    <div class="card text-center">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">

                                    <thead>
                                        <tr>
                                            <th>Nombre(s)</th>
                                            <th>Ap_Paterno</th>
                                            <th>Ap_Materno</th>
                                            <th>Telefono</th>
                                            <th>Correo</th>
                                            <th>Sexo</th>
                                            <th>Estado</th>
                                            <th>Cedula_profesional</th>
                                            <th>Fecha ingreso</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                      @if($profesor->count())
                                      @foreach($profesor as $p)
                                        <tr>
                                            <td>{{$p->Nombre}}</td>
                                            <td>{{$p->Ap_paterno}}</td>
                                            <td>{{$p->Ap_materno}}</td>
                                            <td>{{$p->Telefono}}</td>
                                            <td>{{$p->Correo}}</td>
                                            <td>{{$p->Sexo}}</td>
                                            <td>{{$p->Estatus}}</td>
                                            <td>{{$p->Cedula_profesional}}</td>
                                            <td>{{$p->Fecha_ingreso}}</td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <a href="{{route('editar_profesor', $p->Clave_profesor)}}">
                                                      <button class="item" data-toggle="tooltip" data-placement="top" title="Editar">
                                                        <i class="zmdi zmdi-edit"></i>
                                                      </button>
                                                    </a>
                                                    <a href="{{route('destroy_profesor', $p->Clave_profesor)}}"><button  class="item" data-toggle="tooltip" data-placement="top" title="Eliminar">
                                                    <i class="zmdi zmdi-delete"></i>
                                                    </button></a>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr class="spacer"></tr>

                                        @endforeach
                                        @else
                                        @endif
                                    </tbody>
                                    <!--Table body-->
                                </table>
                                <!--Table-->
                            </div>
                        </div>
                        <div class="card-footer text-muted"></div>
                    </div>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
