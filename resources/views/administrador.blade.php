@extends('layouts.plantilla-menu')
@section('content')
<div class="container">
    <div class="row">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Plan de Estudios</h5>
        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 10rem;" src="img/aula.svg" alt="">
        <p class="card-text">Ir a configuracion de plan de estudios.</p>
        <a href="{{ route('tabla_plan') }}" class="btn btn-primary">ir &rarr;</a>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Coordinadores</h5>
        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 10rem;" src="img/coordinacion.svg" alt="">
        <p class="card-text">Ir a configuracion de coordinadores.</p>
        <a href="{{ route('tabla_coordinador') }}" class="btn btn-primary">ir &rarr;</a>
      </div>
    </div>
  </div>
</div>
<div class="row">
<div class="col-sm-6">
<div class="card">
  <div class="card-body">
    <h5 class="card-title">Profesores</h5>
    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 10rem;" src="img/presentacion.svg" alt="">
    <p class="card-text">Ir a configuracion de .</p>
    <a href="{{ route('tabla_profesor') }}" class="btn btn-primary">ir &rarr;</a>
  </div>
</div>
</div>
<div class="col-sm-6">
<div class="card">
  <div class="card-body">
    <h5 class="card-title">Alumnos</h5>
    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 10rem;" src="img/estudiante.svg" alt="">
    <p class="card-text">Ir a alumnos.</p>
    <a href="{{ route('tabla_alumno') }}" class="btn btn-primary">ir &rarr;</a>
  </div>
</div>
</div>
</div>
@endsection
