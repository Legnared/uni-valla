@extends('layouts.plantilla-menu')
@section('content')
<div class="main-content">
    <div class="section__content section__content--p1250">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <h3 class="title-12 m-b-12">Tabla de Plan de Estudios
                    </h3>
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                        </div>
                        <div class="table-data__tool-right">
                            <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                <i class="zmdi zmdi-plus"></i>Agregar Usuario</button>
                            <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                            </div>
                        </div>
                    </div>
                    <!-- Start Table -->
                    <div class="card text-center">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre(s)</th>
                                            <th>Ap_Paterno</th>
                                            <th>Ap_Materno</th>
                                            <th>Telefono</th>
                                            <th>Correo</th>
                                            <th>Red_Social</th>
                                            <th>Sexo</th>
                                            <th>Estado</th>
                                            <th>Fecha_A</th>
                                            <th>Fecha_E</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>18026410</td>
                                            <td>Lori</td>
                                            <td>Hernandez</td>
                                            <td>Lopez</td>
                                            <td>4432183105</td>
                                            <td>lori@example.com
                                            </td>
                                            <td>Lory Galaxys</td>
                                            <td>M</td>
                                            <td>Activo</td>
                                            <td>29-Marzo-2019:12:15:PM</td>
                                            <td>29-Marzo-2019:1:15:AM</td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Editar">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </button>
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Eliminar">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="spacer"></tr>
                                    </tbody>
                                    <!--Table body-->
                                </table>
                                <!--Table-->
                            </div>
                        </div>
                        <div class="card-footer text-muted"></div>
                    </div>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
