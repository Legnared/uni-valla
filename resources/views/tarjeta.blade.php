@extends('layouts.plantilla-menu')
@section('content')
    <div class="container">
<div class="section__content section__content--p30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="overview-wrap">
                    <h2 class="title-1">Inicio</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <br>
        </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Plan de Estudios</h5>
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 10rem;" src="images/aula.svg" alt="">
                    <p class="card-text">Ir a configuracion de plan de estudios.</p>
                    <a href="#" class="btn btn-primary">ir &rarr;</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Carreras</h5>
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 10rem;" src="images/coordinacion.svg" alt="">
                    <p class="card-text">Ir a configuracion de coordinadores.</p>
                    <a href="#" class="btn btn-primary">ir &rarr;</a>
                </div>
            </div>
        </div>
    </div>
<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Coordinadores</h5>
                <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 10rem;" src="images/presentacion.svg" alt="">
                <p class="card-text">Ir a configuracion de .</p>
                <a href="#" class="btn btn-primary">ir &rarr;</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Profesores</h5>
                <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 10rem;" src="images/presentacion.svg" alt="">
                <p class="card-text">Ir Configuración de profesores.</p>
                <a href="#" class="btn btn-primary">ir &rarr;</a>
            </div>
        </div>
    </div>
</div>

    <div class="card text-center">
        <div class="card-header">Alumno
        </div>
        <div class="card-body">
            <h5 class="card-title">Alumnos</h5>
            <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 10rem;" src="images/estudiante.svg" alt="">
                <p class="card-text">Ir a alumnos.</p>
                <a href="#" class="btn btn-primary">ir &rarr;</a>
        </div>
        <div class="card-footer text-muted"></div>
    </div>


    </div>
</div>
@endsection
