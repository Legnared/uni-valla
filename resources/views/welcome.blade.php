<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Universidad Marista - Página de inicio</title>
        <!-- Fonts -->
        <link rel="shortcut icon" type="image/x-icon" href="images/favi.ico">
        <!--favicon-->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="/css/inicio.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
      <div class="banner">
          <div class="bannerTxt">
              SGE-UMVALLA
          </div>
            <a href="{{route('login')}}"><button type="button" class="btn btn-outline-light btn-sm float-right">Administrador</button></a>
      </div>
      <div class="content ">
          <div class="flex-center ">
              &nbsp
              <img src="../images/UNI2.png" alt="">
            </div>
          <div class="button-group " role="group">
              <a href="#"><button type="button" class="btn btn-primary btn-m" data-toggle="modal" data-target="#modalLoginFormControlEsc">Control Escolar</button></a>
                &nbsp
              <a href="#"><button type="button" class="btn btn-primary btn-m" data-toggle="modal" data-target="#modalLoginFormCoordinador">Coordinador</button></a>
                &nbsp
                <a href="#"><button type="button" class="btn btn-primary btn-m" data-toggle="modal" data-target="#modalLoginFormProfesor">Profesor</button></a>
                &nbsp
                <a href="#"><button type="button" class="btn btn-primary btn-m" data-toggle="modal" data-target="#modalLoginFormAlumno">Alumnos</button></a>
          </div>
        </div>

<!--InicioModalLoginControlEscolar-->
<div class="modal fade" id="modalLoginFormControlEsc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">

        <h4 class="modal-title w-100 font-weight-bold">Login Control Escolar</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
          <form>
    <div class="form-group">
      <label for="formGroupInputClave">Nombre</label>
      <input type="text" class="form-control" id="ClaveInput" placeholder="Clave de Acceso">
    </div>
    <div class="form-group">
      <label for="formGroupInputPassword">Ingrese Contraseña</label>
      <input type="password" class="form-control" id="ContraseñaInput" placeholder="Contraseña">
    </div>
  </form>
      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="submit" class="btn btn-primary">Iniciar Sesión</button>
      </div>
    </div>
  </div>
</div>
<!--finLoginFlotanteControlEscolar-->
<!--inicioModalLoginCoordinador-->
<div class="modal fade" id="modalLoginFormCoordinador" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">

        <h4 class="modal-title w-100 font-weight-bold">Login Coordinador</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
          <form>
    <div class="form-group">
      <label for="formGroupInputClave">Ingrese Clave de Acceso</label>
      <input type="text" class="form-control" id="ClaveInput" placeholder="Clave de Acceso">
    </div>
    <div class="form-group">
      <label for="formGroupInputPassword">Ingrese Contraseña</label>
      <input type="password" class="form-control" id="ContraseñaInput" placeholder="Contraseña">
    </div>
  </form>
      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="submit" class="btn btn-primary">Iniciar Sesión</button>
      </div>
    </div>
  </div>
</div>
<!--FinModalLoginCoordinador-->
<!--InicioModalLoginProfesor-->
<div class="modal fade" id="modalLoginFormProfesor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Login Profesor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
          <form>
    <div class="form-group">
      <label for="formGroupInputClave">Ingrese Clave de Acceso</label>
      <input type="text" class="form-control" id="ClaveInput" placeholder="Clave de Acceso">
    </div>
    <div class="form-group">
      <label for="formGroupInputPassword">Ingrese Contraseña</label>
      <input type="password" class="form-control" id="ContraseñaInput" placeholder="Contraseña">
    </div>
  </form>
      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="submit" class="btn btn-primary">Iniciar Sesión</button>
      </div>
    </div>
  </div>
</div>
<!--finModalLoginProfesor-->
<!--InicioModalLoginAlumno-->
<div class="modal fade" id="modalLoginFormAlumno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">

<<<<<<< HEAD
                <div class="links">
                    <a href="{{ route('login') }}">Control Escolar</a>
                    <a href="#">Alumnos</a>
                    <a href="#">Docente</a>
                    <a href="#">Cordinador</a>
                </div>
            </div>
=======
        <h4 class="modal-title w-100 font-weight-bold">Login Alumnos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
          <form>
    <div class="form-group">
      <label for="formGroupInputClave">Ingrese Clave de Acceso</label>
      <input type="text" class="form-control" id="ClaveInput" placeholder="Clave de Acceso">
    </div>
    <div class="form-group">
      <label for="formGroupInputPassword">Ingrese Contraseña</label>
      <input type="password" class="form-control" id="ContraseñaInput" placeholder="Contraseña">
    </div>
  </form>
      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="submit" class="btn btn-primary">Iniciar Sesión</button>
      </div>
    </div>
  </div>
</div>
<!--finModalLoginAlumno-->
        <div class="footer">
          <br/>
          <h6><a href="https://drive.google.com/file/d/1evzSmdGnDBSmGCuyy0Yu0t18SBREAAQL/view?usp=sharing" class="text-primary">Aviso de privacidad</a></h6>
>>>>>>> 761ebb6e76815341d23f78b7da1968cfbcf21e53
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
