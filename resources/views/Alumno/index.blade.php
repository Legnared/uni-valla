
@extends('layouts.layout')
@section('content')
<div class="row">
  <section class="content">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="pull-left"><h3>Lista  de Alumnos</h3></div>
          <div class="pull-right">
            <div class="btn-group">
              <a href="{{ route('alumno.create') }}" class="btn btn-info" >Añadir Alumno</a>
            </div>
          </div>
          <div class="table-container">
            <table id="mytable" class="table table-bordred table-striped">
             <thead>
               <th>Matricula</th>
               <th>Nombre</th>
               <th>Ap_Paterno</th>
               <th>Ap_Materno</th>
               <th>Telefono</th>
               <th>Correo</th>
               <th>Facebook</th>
               <th>Sexo</th>
               <th>Estatus</th>
               <th>Fecha_Alta</th>
                <th>Fecha_Egreso</th>
                <th>Id_carrera</th>
                  <th>Editar</th>
                <th>Eliminar</th>
             </thead>
             <tbody>
              @if($alumnos->count())
              @foreach($alumnos as $alumno)
              <tr>
                <td>{{$alumno->Matricula}}</td>
                <td>{{$alumno->Nombre}}</td>
                <td>{{$alumno->Ap_Paterno}}</td>
                <td>{{$alumno->Ap_Materno}}</td>
                <td>{{$alumno->Telefono}}</td>
                <td>{{$alumno->Correo}}</td>
                <td>{{$alumno->Facebook}}</td>
                <td>{{$alumno->Sexo}}</td>
                <td>{{$alumno->Estatus}}</td>
                <td>{{$alumno->Fecha_Alta}}</td>
                <td>{{$alumno->Fecha_Egreso}}</td>
                <td>{{$alumno->Id_carrera}}</td>
                <td><a class="btn btn-primary btn-xs" href="{{action('AlunoController@edit', $alumno->id)}}" ><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>
                  <form action="{{action('AlumnoController@destroy', $alumno->id)}}" method="post">
                   {{csrf_field()}}
                   <input name="_method" type="hidden" value="DELETE">

                   <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </td>
               </tr>
               @endforeach
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
            </tbody>

          </table>
        </div>
      </div>
      {{ $alumnos->links() }}
    </div>
  </div>
</section>

@endsection
