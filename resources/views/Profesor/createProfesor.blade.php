<!--
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="<?php echo asset('css/profesor.css')?>" type="text/css">

  </head>
  <body>

@extends('layouts.layout')
@section('content')
<div class="row">
	<section class="content">
		<div class="col-md-8 col-md-offset-2">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Nuevo Profesor</h3>
				</div>
				<div class="panel-body">
					<div class="table-container">
						<form method="POST" action="{{ route('storeProfesor') }}"  role="form">
              @if(session()->has('profe') ){
                {{session('profe')}}
              }
              @endif

              {{ csrf_field() }}
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text"  name="nombre" title="Formato: De 3 a 30 letras" autocomplete="off" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,15}"  id="nombre" class="form-control input-sm" placeholder="Nombre">
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" name="apP" id="apP" title="Formato: De 3 a 45 letras" autocomplete="off" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]{3,15}"  class="form-control input-sm"  placeholder="Apellido Paterno">
									</div>
								</div>
							</div>


              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <input type="text" name="apM" id="apM" title="Formato: De 3 a 45 letras" autocomplete="off" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ]{3,15}"   class="form-control input-sm" placeholder="Apellido Materno">
                  </div>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <input type="text" name="telefono" title="Formato: Maximo 9 numeros" required pattern="[0-9]{0,9}" autocomplete="off" id="teléfono" class="form-control input-sm" placeholder="Telefono">
                  </div>
                </div>
              </div>




							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group" {{$errors->has('correo')}}>
										<input type="email" name="correo" id="correo" required autocomplete="off" class="form-control input-sm" placeholder="Correo">
									</div>
								</div>

                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <input type="text" name="cedula" id="cedula" autocomplete="off" class="form-control input-sm" required placeholder="Cedula profesional">
                  </div>
                </div>

							</div>

              <div class="row">



                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="">

                    <label for="Sexo">Mascculino</label>
                    <input type="radio" value="S" required id="sexo" name="sexo">
                  </div>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="">

                    <label for="Sexo">Femenino</label>
                    <input type="radio" value="M" id="sexo" name="sexo">
                  </div>
                </div>

              </div>

              <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                  <input type="hidden" name="estatus" id="Estatus" value="1" class="form-control input-sm" placeholder="Estatus">
                </div>
              </div>

							<div class="row">

								<div class="col-xs-12 col-sm-12 col-md-12">
									<input type="submit"  value="Guardar" class="btn btn-success btn-block">
									<a href="{{ route('indexProfesor') }}" class="btn btn-info btn-block" >Atrás</a>
								</div>

							</div>



						</form>
            <input name="post" type="hidden" value="PATCH">
					</div>
				</div>

			</div>
		</div>
	</section>
	@endsection

  </body>
</html>
-->
