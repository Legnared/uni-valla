<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

@extends('layouts.layout')
@section('content')
<div class="row">
	<section class="content">
		<div class="col-md-8 col-md-offset-2">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Editar Profesor</h3>
				</div>
				<div class="panel-body">
					<div class="table-container">
                @foreach($profesor as $p)
						<form method="POST" action="{{ route('updateProfesor',$p->Clave_profesor) }}"  role="form">

              @if(session()->has('profe') ){
                {{session('profe')}}
              }
              @endif

              {{ csrf_field() }}

							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text"  name="nombre" value="{{$p->Nombre}}" id="nombre" class="form-control input-sm"  title="Formato: De 3 a 30 letras" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,15}" placeholder="Nombre">
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" name="apP" id="apP" value="{{$p->Ap_paterno}}"  class="form-control input-sm"  title="Formato: De 3 a 45 letras" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,15}" placeholder="Apellido Paterno">
									</div>
								</div>
							</div>


              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <input type="text" name="apM" id="apM" value="{{$p->Ap_materno}}" class="form-control input-sm" title="Formato: De 3 a 15 letras" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}" placeholder="Apellido Materno">
                  </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <input type="text" name="telefono" value="{{$p->Telefono}}" id="telefono" title="Formato: De 7 a 15 números" required pattern="[0-9]{7,15}" class="form-control input-sm" placeholder="Telefono">
                  </div>
                </div>
              </div>




							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group" {{$errors->has('correo')}}>
										<input type="text" name="correo" value="{{$p->Correo}}" id="correo" required class="form-control input-sm" placeholder="Correo">
									</div>
								</div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <input type="text" name="cedula" value="{{$p->Cedula_profesional}}" id="cedula" required class="form-control input-sm" placeholder="Cedula profesional">
                  </div>
                </div>

							</div>




							<div class="row">






								<div class="col-xs-12 col-sm-12 col-md-12">
									<input type="submit"  value="Guardar" class="btn btn-success btn-block">
									<a href="{{ route('indexProfesor') }}" class="btn btn-info btn-block" >Atrás</a>
								</div>

							</div>


            @endforeach
						</form>
            <input name="post" type="hidden" value="PATCH">
					</div>
				</div>

			</div>
		</div>
	</section>
	@endsection

  </body>
</html>
