
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

@extends('layouts.layout')
@section('content')
<div class="row">
  <section class="content">
    <div class="col-md-9 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="pull-left"><h3>Profesores</h3></div>
          <div class="pull-right">
            <div class="btn-group">
              <a href="{{ route('createProfesor') }}" class="btn btn-info" >Añadir Profesores</a>
            </div>
          </div>
          <div class="table-container">
            <table id="mytable" class="table table-bordred table-striped">
             <thead>
               <th>Clave</th>
               <th>Nombre</th>
               <th>Apellido Paterno</th>
               <th>Apellido Materno</th>
               <th>Telefono</th>
               <th>Correo</th>
               <th>Sexo</th>
               <th>Cédula Profesional</th>
               <th>Estatus</th>


               <th>Editar</th>
               <th>Eliminar</th>
             </thead>
             <tbody>
              @if($profesor->count())
              @foreach($profesor as $p)
              <tr>
                <td>{{$p->Clave_profesor}}</td>
                <td>{{$p->Nombre}}</td>
                <td>{{$p->Ap_paterno}}</td>
                <td>{{$p->Ap_materno}}</td>
                <td>{{$p->Telefono}}</td>
                <td>{{$p->Correo}}</td>
                <td>{{$p->Sexo}}</td>
                <td>{{$p->Cedula_profesional}}</td>
                <td>{{$p->Estatus}}</td>


                <td><a class="btn btn-primary btn-xs" href="{{route('editProfesor', $p->Clave_profesor)}} " ><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>
                  <form action="" method="post">
                    {{method_field('DELETE')}}
                   {{csrf_field()}}
                   <input name="_method" type="hidden" value="DELETE">
               <td><a class="btn btn-danger btn-xs" href="{{route('destroyProfesor', $p->Clave_profesor)}} " ><span class="glyphicon glyphicon-trash"></span></td>
                 </td>
               </tr>
               @endforeach
               @else
               <tr>
                <td colspan="8">No hay registros!!</td>
              </tr>
              @endif
            </tbody>

          </table>
        </div>
      </div>

    </div>
  </div>
</section>

@endsection
  </body>
</html>
