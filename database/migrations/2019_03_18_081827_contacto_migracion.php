<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContactoMigracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacto', function (Blueprint $table) {
            $table->increments('Id_contacto');
            $table->string('Nombre',30);
            $table->string('Ap_paterno',45);
            $table->string('Ap_materno',45);
            $table->string('Telefono',10);
            $table->string('Correo',45);
            $table->integer('Id_alumno')->unsigned();
            $table->foreign('Id_alumno')->references('Matricula')->on('alumno');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacto');
    }
}
