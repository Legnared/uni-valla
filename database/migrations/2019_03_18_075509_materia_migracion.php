<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MateriaMigracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materia', function (Blueprint $table) {
            $table->increments('Clave_materia');
            $table->string('Nombre');
            $table->integer('Clave_rveo')->lenght(8)->unsigned();
            $table->integer('No_creditos')->lenght(2)->unsigned();
            $table->integer('Semestre')->lenght(2)->unsigned();
            $table->integer('No_horas')->lenght(3)->unsigned();
            $table->boolean('Estatus');
            $table->integer('Id_carrera')->unsigned();  //Llave foranea de carrera
            $table->foreign('Id_carrera')->references('Id_carrera')->on('carrera');
            $table->integer('Id_plan')->unsigned();  //Llave foranea de plan de estudios
            $table->foreign('Id_plan')->references('Id_plan')->on('plan_estudios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materia');
    }
}
