<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GrupoHistoricoMigracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_historico', function (Blueprint $table) {
            $table->increments('Clave_grupo');
            $table->string('Descripcion');
            $table->string('Salon',3);
            $table->string('Horario',11);
            $table->integer('Clave_profesor')->unsigned();  //Llave profesor
            $table->foreign('Clave_profesor')->references('Clave_profesor')->on('profesor');
            $table->integer('Clave_materia')->unsigned();   //Llave materia
            $table->foreign('Clave_materia')->references('Clave_materia')->on('materia');
            $table->integer('Id_periodo')->unsigned();   //Llave de periodo
            $table->foreign('Id_periodo')->references('Id_periodo')->on('periodo_historico');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo_historico');
    }
}
