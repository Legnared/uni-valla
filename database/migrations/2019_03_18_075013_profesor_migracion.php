<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProfesorMigracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesor', function (Blueprint $table) {
            $table->increments('Clave_profesor');
            $table->string('Nombre',30);
            $table->string('Ap_paterno',45);
            $table->string('Ap_materno',45);
            $table->string('Telefono',10);
            $table->string('Correo',45);
            $table->enum('Sexo', ['M', 'F']);
            $table->string('Cedula_profesional', 35);
            $table->date('Fecha_ingreso');
            $table->boolean('Estatus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profesor');
    }
}
