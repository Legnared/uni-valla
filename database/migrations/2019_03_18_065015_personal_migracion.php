<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PersonalMigracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coordinador', function (Blueprint $table) {
            $table->increments('Clave');
            $table->string('Nombre',30);
            $table->string('Ap_paterno',30);
            $table->string('Ap_materno',30);
            $table->string('Telefono',10);
            $table->string('Correo',45);
            $table->enum('Sexo', ['M', 'F']);	
            $table->string('Password', 15);
            $table->string('updated_at', 20);
            $table->string('created_at', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coordinadors');
    }
}
