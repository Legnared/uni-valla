<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GrupoMigracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo', function (Blueprint $table) {
            $table->increments('Clave_grupo');
            $table->string('Descripcion');
            $table->string('Salon',3);
            $table->integer('Id_horario')->unsigned();  //Llave profesor
            $table->foreign('Id_horario')->references('Id_horario')->on('horario');
            $table->integer('Clave_profesor')->unsigned();  //Llave profesor
            $table->foreign('Clave_profesor')->references('Clave_profesor')->on('profesor');
            $table->integer('Clave_materia')->unsigned();   //Llave materia
            $table->foreign('Clave_materia')->references('Clave_materia')->on('materia');
            $table->integer('Id_periodo')->unsigned();   //Llave de periodo
            $table->foreign('Id_periodo')->references('Id_periodo')->on('periodo_historico');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo');
    }
}
