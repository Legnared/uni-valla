<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlumnoMigracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumno', function (Blueprint $table) {
            $table->increments('Matricula');
            $table->string('Nombre',30);
            $table->string('Ap_Paterno',45);
            $table->string('Ap_Materno',45);
            $table->string('Telefono',10);
            $table->string('Correo',45);
            $table->string('Facebook', 45);
            $table->enum('Sexo', ['M', 'F']);
            $table->boolean('Estatus');
            $table->date('Fecha_Alta');
            $table->date('Fecha_Egreso');
            $table->integer('Id_carrera')->unsigned();
            $table->foreign('Id_carrera')->references('Id_carrera')->on('carrera');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}
