<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KardexMigracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kardex', function (Blueprint $table) {
            $table->increments('id_Kardex');
            $table->double('Parcial_1',3,1);
            $table->double('Parcial_2',3,1);
            $table->double('Parcial_3',3,1);
            $table->double('Promedio',3,2);
            $table->integer('faltas');
            $table->string('Opcion',15);
            $table->double('Calificacion_final',3,2);
            $table->double('Reg',45);
            $table->date('Fecha');
            $table->integer('Matricula')->unsigned();   //Llave materia
            $table->foreign('Matricula')->references('Matricula')->on('alumno');
            $table->integer('Clave_grupo')->unsigned();   //Llave de periodo
            $table->foreign('Clave_grupo')->references('Clave_grupo')->on('grupo_historico');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kardex');
    }
}
