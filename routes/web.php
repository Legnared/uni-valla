<?php


Route::get('/', function () {
    return view('welcome');
})->name('regresar');

Route::get('/plantilla-menu', function(){
    return view('plantilla-menu');
});

Route::get('/plantilla-login', function(){
    return view('plantilla-login');
});

Route::get('/administrador', function(){
    return view('administrador');
})->name('admin');

Route::get('/bienvenido-control-escolar', function(){
    return view('bienvenido-control-escolar');
})->name('bienvenida-admin');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


/*Profesores*/

  /*Crear Profesor: crear->llama a la vista, store->almacena en la BD     */
  Route::get('/crear_profesor', 'ProfesorController@create')->name('crear_profesor');
  Route::post('/almacenar_profesor', 'ProfesorController@store')->name('almacenar_profesor');
  /*FIn crear profesor*/

  /*Tabla profesor*/
  Route::get('/tabla_profesor', 'ProfesorController@index')->name('tabla_profesor');
  Route::get('/registro_crud_profesor', 'ProfesorController@index')->name('crud_profesor');

  /*Editar profesor*/
  Route::get('/editar_profesor{Clave_profesor}','ProfesorController@edit')->name('editar_profesor');
  Route::post('/updateProfesor{Clave_profesor}', 'ProfesorController@update')->name('update_profesor');


  /*Eliminar Profesores*/
  Route::get('/destroyProfesor{Clave_profesor}', 'ProfesorController@destroy')->name('destroy_profesor');

/*
  Route::get('/registro_crud_profesores', function(){
      return view('registro_crud_profesores');
  })->name('crud_profesor');
*/

/*Fin profesores*/




/*Coordinadores*/
Route::get('/coordinadores', function(){
    return view('coordinadores');
})->name('coordinador');

Route::get('/registro_crud_coordinadores', function(){
    return view('registro_crud_coordinadores');
})->name('crud_coordinador');

Route::resource('coordinador', 'CoordinadorController');

Route::get('/tabla_coordinadores', function(){
    return view('tabla_coordinadores');
})->name('tabla_coordinador');
/*Fin coordinadores*/


/*Alumnos*/

  Route::get('/registro_crud_alumnos', function(){
    return view('registro_crud_alumnos');
  })->name('crud_alumno');

  Route::get('/tabla_alumnos', function(){
      return view('tabla_alumnos');
  })->name('tabla_alumno');


/*Fin alumnos*/



/*Plan de estudios*/
Route::get('/tabla_plan_es', function(){
    return view('tabla_plan_es');
})->name('tabla_plan');
/*Plan de estudios*/







/*
<<<<<<< HEAD


Route::resource('index', 'AlumnoController');


Route::get('/menuProfesor', 'ProfesorController@menu')->name('menuProfesor');
Route::get('/indexProfesor', 'ProfesorController@index')->name('indexProfesor');
Route::get('/createProfesor', 'ProfesorController@create')->name('createProfesor');
Route::get('/editProfesor{Clave_profesor}', 'ProfesorController@edit')->name('editProfesor');
Route::post('/storeProfesor', 'ProfesorController@store')->name('storeProfesor');
Route::post('/updateProfesor{Clave_profesor}', 'ProfesorController@update')->name('updateProfesor');
  Route::get('/destroyProfesor{Clave_profesor}', 'ProfesorController@destroy')->name('destroyProfesor');
=======
>>>>>>> 761ebb6e76815341d23f78b7da1968cfbcf21e53
*/
