<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profesor;

class ProfesorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
/*
     public function __construct()
     {
         $this->middleware('auth');
     }
*/


    public function index()
    {
       //$profes=ProfesorController::orderBy('id','DESC')->paginate(3);
       $profesor = DB::table('profesor')->get();
        return view('tabla_profesores',compact('profesor'));
      // return DB::table('profesor')->get();


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
      return view("registro_crud_profesores");
    }

    public function menu()
    {
        return view('Profesor/menuProfesor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    $request->validate([
      'nombre' => 'min:5'

    ]);

    DB::table('profesor')->insert([
      'Nombre' => $request->nombre,
      'Ap_paterno' => $request->apP,
      'Ap_materno' => $request->apM,
      'Telefono' => $request->telefono,
      'Correo' => $request->correo,
      'Sexo' => $request->sexo,
      'Cedula_profesional' => $request->cedula,
      'Fecha_ingreso' => now(),
      'Estatus' => $request->estatus
    ]);

     return back()->with('profe','Registro correcto');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $clave = (int) $id;
      //$profesor =  DB::table('profesor')->where('Clave_profesor','=',$id)->get()->first();
      $profesor =  DB::table('profesor')->where('Clave_profesor','=',$id)->get()->first();
      //return $a;
      //return $profesor;
      return view('edicion_crud_profesor', compact('profesor'));


    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profesor =  DB::table('profesor')->where('Clave_profesor','=',$id)->update([
        'Nombre'=>$request->nombre,
        'Ap_paterno' => $request->apP,
        'Ap_materno' => $request->apM,
        'Telefono' => $request->telefono,
        'Correo' => $request->correo,
        'Cedula_profesional' => $request->cedula

        ]);
         return back()->with('profe','Profesor actualizado correctamente');
        //return back()->with('Profe', 'Registro actualizado');

        //return $request;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $status = 0;
      $profesor =  DB::table('profesor')->where('Clave_profesor','=',$id)->update(['Estatus'=>$status]);
       return back()->with('profe','Registro eliminado');
    }
}
