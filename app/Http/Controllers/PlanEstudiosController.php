<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\plan_estudios;

class PlanEstudiosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $plan_estudios=plan_estudios::orderBy('Id_plan','DESC')->paginate(3);
        return view('Plan_Estudios.index',compact('plan_estudios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('plan_estudios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[ 'Id_plan'=>'required', 'Descripcion'=>'required', 'id_Carrera'=>'required', 'MATERIA_Clave_Materia'=>'required']);
        //$plan_estudios = plan_estudios::create('Id_plan':$request, 'Descripcion':$request, 'id_Carrera':$request, 'MATERIA_Clave_Materia':$request);
  $plan_estudios = plan_estudios::create($request->all());

        return redirect()->route('plan_estudios.index')->with('success','Registro creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($Id_plan)
    {
        //
        $plan_estudios=plan_estudios::find($Id_plan);
        return  view('plan_estudios.show',compact('plan_estudios'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($Id_plan)
    {
        //
        $plan_estudios=plan_estudios::find($Id_plan);
        return view('plan_estudios.edit',compact('plan_estudios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $Id_plan)
    {
        //
        $this->validate($request,[ 'Id_plan'=>'required', 'Descripcion'=>'required', 'id_Carrera'=>'required', 'MATERIA_Clave_Materia'=>'required']);
       plan_estudios::find($Id_plan)->update($request->all());
       return redirect()->route('Plan_Estudios.index')->with('success','Registro actualizado satisfactoriamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($Id_plan)
    {
        //
        plan_estudios::find($Id_plan)->delete();
      return redirect()->route('Plan_Estudios.index')->with('success','Registro eliminado satisfactoriamente');

    }
}
