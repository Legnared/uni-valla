<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\coordinador;

class CoordinadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('coordinadores');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[ 'Nombre'=>'required', 'Ap_Paterno'=>'required', 'Ap_Materno'=>'required', 'Telefono'=>'required', 'Correo'=>'required', 'Sexo'=>'required', 'Password'=>'required']);
        //$plan_estudios = plan_estudios::create('Id_plan':$request, 'Descripcion':$request, 'id_Carrera':$request, 'MATERIA_Clave_Materia':$request);
  $coordinador = coordinador::create($request->all());

        return redirect()->route('coordinador.index')->with('success','Registro creado');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
