<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Alumno;

class AlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function __construct()
     {
         $this->middleware('auth');
     }




    public function index()
    {
        //
        $alumno=DB::table(alumno)->orderBy('id','DESC')->paginate(3);
        return view('Alumno.index',compact('$alumnos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Alumno.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,['Matricula'=>'require','Nombre'=>'require','Ap_Paterno'=>'require','Ap_Materno'=>'require','Telefono'=>'require','Correo'=>'require','Sexo'=>'require','Estatus'=>'require','Fecha_Alta'=>'require','Fecha_Egreso'=>'require','Id_carrera'=>'require']);
        Alumno::create($request->all());
        return redirect()->route('alumno.index')->with('success','registro creado.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $alumnos=Alumno::find($id);
        return view('Alumno.index',compact('alumnos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $alumno=alumno::find($id);
        return view('alumno.edit',compact('alumno'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Alumno::find($id)->delete();
        return redirect()->route('alumno.index')->with('success','Registro eliminado satisfactori');
    }
}
