<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Profesor extends Model
{
  
    protected $fillable = ['Clave_profesor','Nombre','Ap_paterno','Ap_materno','Telefono','Correo','Sexo','Cedula_profesional','Fecha_ingreso','Estatus'];
}
