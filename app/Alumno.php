<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Alumno extends Model
{
    //
     protected $fillable = ['Matricula','Nombre', 'Ap_Paterno', 'Ap_Materno','Telefono','Correo','Facebook','Sexo','Estatus','Fecha_Alta','Fecha_Egreso','Id_carrera'];
}
